package soli.test.task.services;

import soli.test.task.model.User;

import java.util.List;

public interface UserService {
    User saveOrUpdate(User user);

    User getById(String id);

    void delete(String id);

    List<User> getAll();

    void signUp(User user);
}

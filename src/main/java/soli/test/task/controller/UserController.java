package soli.test.task.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import soli.test.task.model.User;
import soli.test.task.services.UserService;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/create")
    public User create(@RequestBody User user) {
        return userService.saveOrUpdate(user);
    }

    @GetMapping("/user/{id}")
    public User getById(@RequestParam String id) {
        return userService.getById(id);
    }

    @GetMapping("/users")
    public List<User> getAll() {
        return userService.getAll();
    }

    @PutMapping("/update")
    public User update(@RequestBody User user) {
        return userService.saveOrUpdate(user);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable String id) {
        userService.delete(id);
    }
}

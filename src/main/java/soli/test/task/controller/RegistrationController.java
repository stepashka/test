package soli.test.task.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import soli.test.task.model.User;
import soli.test.task.services.UserService;

@RestController
@RequiredArgsConstructor
public class RegistrationController {

    private final UserService userService;

    @PostMapping("/register")
    public void signUp(@RequestBody User user) {
        userService.signUp(user);
    }
}
